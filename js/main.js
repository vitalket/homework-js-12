/*
    Eсли мы хотим корректно отслеживать ввод в поле <input>, то одних клавиатурных событий недостаточно. 
    Существует специальное событие input, чтобы отслеживать любые изменения в поле <input>. 
    И оно справляется с такой задачей намного лучше.
 */
window.addEventListener('DOMContentLoaded', () => {
    const buttons = document.querySelectorAll('.btn');

    document.addEventListener('keydown', (event) => {
        const key = event.key.toUpperCase();
        const button = Array.from(buttons).find(btn => btn.textContent.toUpperCase() === (key === 'ENTER' ? 'ENTER' : key));

        if (button) {
            buttons.forEach(btn => {
                btn.classList.remove('blue');
                btn.classList.add('black');
            });

            button.classList.remove('black');
            button.classList.add('blue');
        }
    });
});

















